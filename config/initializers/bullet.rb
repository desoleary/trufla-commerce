# frozen_string_literal: true

if defined? Bullet
  Bullet.enable = true
  Bullet.alert = true
  Bullet.bullet_logger = true
  Bullet.console = ENV.fetch('VERBOSE_BULLET_OUTPUT', 'false') == 'true'
  Bullet.rails_logger = true
  Bullet.add_footer = true
end
