# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.2.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 3.11'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'active_model_serializers', '~> 0.10.8'
gem 'bootsnap', '>= 1.1.0', require: false
gem 'factory_bot_rails'
gem 'faker', '~> 1.9', '>= 1.9.1'
gem 'has_scope', '~> 0.7.2'
gem 'jsonapi-rails', '~> 0.3.1'
gem 'kaminari', '~> 1.1', '>= 1.1.1'
gem 'money-rails', '~>1.13'
gem 'pg_search', '~> 2.1', '>= 2.1.2'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]

  gem 'awesome_print', '~> 1.8'
  gem 'cucumber-rails', require: false
  gem 'dotenv-rails', '~> 2.5'
  gem 'guard', '~> 2.15'
  gem 'guard-brakeman', '~> 0.8.3'
  gem 'guard-bundler', '~> 2.1', require: false
  gem 'guard-rails_best_practices', github: 'logankoester/guard-rails_best_practices'
  gem 'guard-reek', '~> 1.2'
  gem 'guard-rspec', '~> 4.7', '>= 4.7.3'
  gem 'guard-rubocop', '~> 1.3'
  gem 'reek', '~> 5.2'
  gem 'rspec-collection_matchers', '~> 1.1', '>= 1.1.3'
  gem 'rspec-json_expectations', '~> 2.1'
  gem 'rspec-rails', '~> 3.5'
  gem 'rspec-support', '~> 3.8'
  gem 'rubocop-rails_config', '~>0.2.6'
  gem 'rubocop-rspec', '~> 1.30', '>= 1.30.1'
  gem 'ruby-growl', '~> 4.1'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'brakeman'
  gem 'bullet', '~> 5.9'
  gem 'bundler-audit', '~> 0.6.0'
  gem 'guard-bundler-audit', '~> 0.1.2'
  gem 'meta_request', '~> 0.6.0'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  gem 'chromedriver-helper'
  gem 'database_cleaner'
  gem 'rails-controller-testing'
  gem 'shoulda-matchers', '4.0.0.rc1'
  gem 'simplecov', require: false
end
