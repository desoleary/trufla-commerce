# frozen_string_literal: true

# A sample Guardfile
# More info at https://github.com/guard/guard#readme

## Uncomment and set this to only include directories you want to watch
# directories %w(app lib config test spec features) \
#  .select{|d| Dir.exist?(d) ? d : UI.warning("Directory #{d} does not exist")}

## Note: if you are using the `directories` clause above and you are not
## watching the project directory ('.'), then you will want to move
## the Guardfile to a watched dir and symlink it back, e.g.
#
#  $ mkdir config
#  $ mv Guardfile config/
#  $ ln -s config/Guardfile .
#
# and, you'll have to watch "config/Guardfile" instead of "Guardfile"

interactor :simple

guard :rubocop, all_on_start: false, cmd: 'bundle exec rubocop --auto-correct --rails' do
  watch(%r{^app/.+\.rb$})
  watch(%r{^config/.+\.rb$})
  watch(%r{^lib/.+\.rb$})
  watch(%r{^spec/.+_spec\.rb})
  watch('Gemfile')
end

guard :rspec, cmd: 'bundle exec rspec',
              all_after_pass: false, all_on_start: false, keep_failed: false do
  watch('spec/spec_helper.rb')                                               { 'spec' }
  watch('app/controllers/application_controller.rb')                         { 'spec/controllers' }
  watch(%r{^spec/support/(requests|controllers|mailers|models)_helpers\.rb}) { |m| "spec/#{m[1]}" }
  watch(%r{^spec/.+_spec\.rb})

  watch(%r{^app/controllers/(.+)_(controller)\.rb}) do |m|
    ["spec/routing/#{m[1]}_routing_spec.rb", "spec/#{m[2]}s/#{m[1]}_#{m[2]}_spec.rb", "spec/requests/#{m[1]}_spec.rb"]
  end

  watch(%r{^app/(.+)\.rb})                                                   { |m| "spec/#{m[1]}_spec.rb" }
  watch(%r{^lib/(.+)\.rb})                                                   { |m| "spec/lib/#{m[1]}_spec.rb" }
end

guard :brakeman, run_on_start: true, quiet: true, output_files: %w[brakeman.html] do
  watch(%r{^app/.+\.(erb|haml|rhtml|rb)$})
  watch(%r{^config/.+\.rb$})
  watch(%r{^lib/.+\.rb$})
  watch('Gemfile')
end

guard :rails_best_practices, vendor: false do
  watch(%r{^app/(.+)\.rb$})
end

guard :bundler do
  require 'guard/bundler'
  require 'guard/bundler/verify'
  helper = Guard::Bundler::Verify.new

  files = ['Gemfile']
  files += Dir['*.gemspec'] if files.any? { |f| helper.uses_gemspec?(f) }

  # Assume files are symlinked from somewhere
  files.each { |file| watch(helper.real_path(file)) }
end

guard 'reek', all: 'app config lib spec' do
  watch('.reek')
  watch(%r{^app/(.+)\.rb})
  watch(%r{^config/(.+)\.rb})
  watch(%r{^lib/(.+)\.rb})
  watch(%r{^spec/(.+)\.rb})
end

guard 'bundler_audit', run_on_start: true do
  watch('Gemfile.lock')
end
