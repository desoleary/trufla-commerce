# frozen_string_literal: true

module ApiJsonPaginate
  extend ActiveSupport::Concern

  included do
    send :include, InstanceMethods
  end

  module InstanceMethods
    def jsonapi_pagination(collection)
      @collection = collection

      return  if disabled?

      links
    end

    private

    def links
      @links = {}
      @links[:self] = request.url
      @links[:first] = url(1)
      @links[:prev] = url(@collection.prev_page) unless @collection.first_page?
      @links[:next] = url(@collection.next_page) unless @collection.last_page? || @collection.out_of_range?
      @links[:last] = url(@collection.total_pages)
      @links
    end

    def url(page)
      params = { page: page, page_size: @collection.limit_value }.merge(request.query_parameters.to_h)
      current_uri_without_params + '?' + params.to_query
    end

    def current_uri_without_params
      request.base_url + request.path
    end

    def disabled?
      !@collection.respond_to?(:first_page?)
    end
  end
end
