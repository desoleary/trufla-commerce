# frozen_string_literal: true

class ProductsController < ApplicationController
  def index
    products = Product.all
    products = products.by_department(params[:by_department]) if params[:by_department]
    products = products.by_promotion(params[:by_promotion]) if params[:by_promotion]
    products = products.search_full_text(params[:search]) if params[:search]
    products = products.page(params[:page])
                       .per(params[:page_size])

    render jsonapi: products
  end
end
