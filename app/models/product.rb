# frozen_string_literal: true

class Product < ApplicationRecord
  include PgSearch

  validates :name, presence: true, length: { minimum: 3, maximum: 255 }, uniqueness: true
  belongs_to :department

  has_one :product_promotion, dependent: :destroy
  has_one :promotion, through: :product_promotion

  monetize :price_cents
  validates :price_cents, numericality: { only_integer: true, greater_than: 0 }

  validates_associated :department

  pg_search_scope :search_full_text, against: [:name]
  scope :by_department, ->(name) { joins(:department).where(departments: { name: name }) }
  scope :by_promotion, ->(code) { joins(:promotion).where(promotions: { code: code }) }

  def price_humanized
    ActionController::Base.helpers.humanized_money_with_symbol price_cents
  end
end
