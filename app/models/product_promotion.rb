# frozen_string_literal: true

class ProductPromotion < ApplicationRecord
  self.table_name = 'product_promotion'

  belongs_to :product
  belongs_to :promotion

  validates_associated :product, :promotion

  # TODO: allow multiple products if there is no overlap in times
  validates :product_id, uniqueness: true
end
