# frozen_string_literal: true

class Department < ApplicationRecord
  validates :name, presence: true, length: { minimum: 3, maximum: 255 }
end
