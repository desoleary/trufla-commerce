# frozen_string_literal: true

class Promotion < ApplicationRecord
  validates :code, presence: true, length: { minimum: 3, maximum: 50 },
                   uniqueness: true

  has_many :product_promotions, dependent: :destroy
  has_many :products, through: :product_promotions

  validates :discount_percentage, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0,
    less_than_or_equal_to: 100
  }
end
