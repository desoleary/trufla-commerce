# frozen_string_literal: true

unless respond_to?(:object_print_filter_methods)

  # Prints and returns the list method names filtered by given search
  def object_print_filter_methods(object, search)
    (object.methods - object.methods).select { |method| method.to_s.include? search }
  end
end
