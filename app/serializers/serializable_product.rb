# frozen_string_literal: true

class SerializableProduct < JSONAPI::Serializable::Resource
  type 'products'

  attributes :name

  attribute :price do
    @object.price_humanized
  end

  attribute :department do
    @object.department.try(:name)
  end

  has_one :promotion do
    meta do
      { code: @object.promotion.try(:code), discount_percentage: @object.promotion.try(:discount_percentage) }
    end
  end
end
