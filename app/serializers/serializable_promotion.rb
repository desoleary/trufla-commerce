# frozen_string_literal: true

class SerializablePromotion < JSONAPI::Serializable::Resource
  type 'promotions'

  has_many :products

  attributes :code, :discount_percentage
end
