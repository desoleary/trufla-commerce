# Trufla Ruby on Rails Code Challenge

## Usage

#### Example API response

```js
{
    "data": [
        {
            "id": "6",
            "type": "products",
            "attributes": {
                "name": "Ergonomic Paper Shirt 6",
                "price": "$9,236",
                "department": "Automotive"
            },
            "relationships": {
            	"data" {
            		"attributes": {
                        "code": "GreatPromotion611531",
                        "discount_percentage": 84
            		}
            	}
            }
        }
    ],
    "links": {
        "self": "http://127.0.0.1:3000/products?page_size=1&page=6",
        "first": "http://127.0.0.1:3000/products?page=1&page=6&page_size=1&page_size=1",
        "prev": "http://127.0.0.1:3000/products?page=5&page=6&page_size=1&page_size=1",
        "next": "http://127.0.0.1:3000/products?page=6&page=7&page_size=1&page_size=1",
        "last": "http://127.0.0.1:3000/products?page=18&page=6&page_size=1&page_size=1"
    },
    "jsonapi": {
        "version": "1.0"
    }
}
```

### Scenarios

#### List Products with default pagination
`GET http://127.0.0.1:3000/Products`

#### Filter Products by department
`GET http://127.0.0.1:3000/products?by_department=Kids%2C+Automotive+%26+Sports`

#### Filter Products by promotion code
`GET http://127.0.0.1:3000/products?by_promotion=CoolSale353802`

#### Filter Products by name partial search
`GET http://127.0.0.1:3000/products?search=Granite Bag`

#### Filter by department and promotion
`GET http://127.0.0.1:3000/products?by_department=Kids%2C+Automotive+%26+Sports&by_promotion=CoolSale353802`

#### Change page
`GET http://127.0.0.1:3000/products?page=2`

#### Change page size
`GET http://127.0.0.1:3000/products?page_size=25`

#### Change page and page size
`GET http://127.0.0.1:3000/products?page_size=1&page=6`

### Heroku Deployment

> Follow [these](https://devcenter.heroku.com/articles/getting-started-with-rails5) steps in order to deploy changes directly to heroku

#### Useful Heroku Commands

`$ heroku console -a powerful-plateau-56437 opens rails console`
$ heroku logs -a powerful-plateau-56437 shows recent logs

$ heroku logs -t -a powerful-plateau-56437 # tail logs

$ heroku config ENVVAR=blah -a application_name add/update config

#### Push changes to Heroku

`git push heroku master`
`git push heroku master:<branch-name>` e.g. git push heroku master:api-setup


#### Refresh Database

` ./script/bash/heroku_db_refresh.sh` run locally only


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

This project is based on Ruby on Rails, review their [installation guide](https://guides.rubyonrails.org/v5.0/getting_started.html#installing-rails) before continuing.

### Developer Guides

[Ruby](https://devdocs.io/ruby/)

[Rails](https://devdocs.io/rails/)

[React](https://reactjs.org/docs/getting-started.html)

[Rematch & Redux](https://github.com/rematch/rematch#getting-started)

[json:api](http://jsonapi-rb.org/guides/)

[json:api Detailed Specification](https://jsonapi.org/)

### Prerequisites
    ruby >= 2.5.3
    PostgreSQL (MariaDB or Postgres)

#### Ruby Steps


###### rbenv

[Installation Guide](https://github.com/rbenv/rbenv#basic-github-checkout)

```bash
git clone https://github.com/rbenv/rbenv.git ~/.rbenv
cd ~/.rbenv && src/configure && make -C src
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
```

`Edit ~/.bash_profile`
```bash
eval "$(rbenv init -)"
```

`Install ruby e.g. 2.5.3`

```
rbenv install 2.5.3
```

#### PostgreSQL Steps

[Getting Started Guide](https://www.codementor.io/engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb)

[Detailed Steps](https://gist.github.com/sgnl/609557ebacd3378f3b72)


```bash
brew install postgresql

ln -sfv /usr/local/opt/postgresql/*.plist ~/Library/LaunchAgents
```

`Edit to ~/.alias`

```
alias pg-start="launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"
alias pg-stop="launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"
```

```pg-start```

###### Troubleshooting

Issue: postgres: could not connect to server: No such file or directory

Solution:
https://gist.github.com/giannisp/ebaca117ac9e44231421f04e7796d5ca

### Installing

Clone the repo from our github:

```bash
git clone git@gitlab.com:desoleary/trufla-commerce.git
cd trufla-commerce

bundle install
```

Run development server

```
bin/rails server -b 0.0.0.0 -p 3000 -e development
```

### Database

`Refresh database` (drops, creates, migrates and then seeds all tables)
```
bin/rake db:refresh && RAILS_ENV=test bin/rake db:refresh
```

### Testing

- Each change needs to be accompanied by a spec

```
bin/rspec
```

### Development

#### Guard

###### Plugins

    guard-brakeman (Ruby on Rails Static Analysis Security Tool)
    guard-bundler (Automatically install/update your gem bundle when needed)
    guard-rspec (automatically run your specs (much like autotest) )
    guard-rubocop (allows you to automatically check Ruby code style with [RuboCop](https://github.com/bbatsov/rubocop) when files are modified.)
    guard-rails_best_practices (a code metric tool to check the quality of rails code)
    guard-reek (Automatically detect code smells with Reek when files are modified.)

Run guard at all times (performs the following tasks)

```
guard
```
