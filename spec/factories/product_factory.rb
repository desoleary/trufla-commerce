# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    sequence(:name) { |n| "#{Faker::Commerce.product_name} #{n}" }
    department
    price_cents { (Faker::Commerce.price * 100).to_i }

    trait :with_promotion do
      after(:build) do |product|
        product.promotion = build(:promotion)
      end
    end
  end
end
