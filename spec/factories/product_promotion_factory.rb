# frozen_string_literal: true

FactoryBot.define do
  factory :product_promotion do
    product
    promotion
  end
end
