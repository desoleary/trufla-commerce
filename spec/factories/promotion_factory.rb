# frozen_string_literal: true

FactoryBot.define do
  factory :promotion do
    code { Faker::Commerce.promotion_code }
    discount_percentage { rand(0..100) }

    starts_at { Time.current }
    expires_at { Time.current + 1.month }

    trait :with_products do
      after(:build) do |product|
        product.products = [build(:product), build(:product)]
      end
    end
  end
end
