# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product do
  describe 'validations' do
    subject { build(:product) }

    it { is_expected.to validate_uniqueness_of(:name).ignoring_case_sensitivity }
    it { is_expected.to belong_to(:department) }
    it { is_expected.to have_one(:promotion) }
    it { is_expected.to monetize(:price_cents) }
    it { is_expected.to validate_numericality_of(:price_cents).is_greater_than(0) }
  end

  describe 'search_full_text' do
    it 'returns full word matches' do
      create_products('Bat', 'batman', 'fake bat', 'blbat', 'non-matching-value')

      expect(described_class.search_full_text('BaT').pluck('name')).to eql(['Bat', 'fake bat'])
    end
  end

  def create_products(*names)
    names.each do |name|
      create(:product, name: name)
    end
  end
end
