# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Promotion do
  describe 'validations' do
    subject { build(:promotion) }

    it { is_expected.to validate_uniqueness_of(:code).ignoring_case_sensitivity }
    it { is_expected.to validate_length_of(:code).is_at_most(50).is_at_least(3) }

    it do
      promotion = build(:promotion)

      expect(promotion).to validate_numericality_of(:discount_percentage)
        .is_greater_than_or_equal_to(0)
        .is_less_than_or_equal_to(100)
    end
  end

  describe '.products' do
    let(:promotion) { create(:promotion, :with_products) }

    it 'returns products associated with promotion' do
      expect(promotion.products).to have_exactly(2).items
    end
  end
end
