# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductPromotion do
  describe 'validations' do
    subject { build(:product_promotion) }

    it { is_expected.to belong_to(:product) }
    it { is_expected.to belong_to(:promotion) }
    it { is_expected.to validate_uniqueness_of(:product_id).ignoring_case_sensitivity }
  end
end
