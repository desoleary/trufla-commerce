# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Department do
  describe 'validations' do
    subject { build(:department) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(255).is_at_least(3) }
  end
end
