# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Products API', type: :request do
  let!('products') { create_list(:product, 10, :with_promotion) }
  let!('product') { Product.first }

  describe 'GET /products' do
    before { get '/products' }

    it 'returns products with pagination' do
      expect(response.status).to eq(200)
      expect(response.body).to be_jsonapi_response_for('products')
      expect(response_body[:data]).to have_exactly(5).items
    end

    it 'returns product data' do
      expect(response_attributes).to include(
        name: product.name,
        price: product.price_humanized,
        department: product.department.name
      )
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(:ok)
    end
  end
end
