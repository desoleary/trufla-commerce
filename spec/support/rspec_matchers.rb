# frozen_string_literal: true

RSpec::Matchers.define :be_jsonapi_response_for do |model|
  match do |actual|
    json = JSON.parse(actual)
    data = json.dig('data')

    assert_json = lambda do |item|
      item.dig('type') == model &&
        item.dig('attributes').is_a?(Hash)
    end

    Array(data).each do |item|
      assert_json.call(item)
    end
  end
end
