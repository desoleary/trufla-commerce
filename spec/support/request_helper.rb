# frozen_string_literal: true

module RequestHelper
  def response_body
    JSON.parse(response.body, symbolize_names: true)
  end

  def response_attributes
    response_body[:data].map { |item| item[:attributes] }
  end
end
