# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApiJsonPaginate do
  include described_class

  describe 'InstanceMethods' do
    describe '#jsonapi_pagination' do
      context 'without pagination' do
        let(:product) { create(:product, :with_promotion) }

        it 'return nil without pagination' do
          expect(jsonapi_pagination(Product.all)).to be_nil
        end
      end

      context 'with pagination' do
        context 'with default pagination' do
          let!(:product) { create(:product, :with_promotion) }

          it 'returns pagination details based on default configuration' do
            base_url = request.base_url

            expect(jsonapi_pagination(Product.page(1))).to eql(
              first: "#{base_url}?page=1&page_size=5",
              last: "#{base_url}?page=1&page_size=5",
              self: base_url
            )
          end
        end

        context 'with multiple pages' do
          let!(:products) { create_list(:product, 6, :with_promotion) }

          it 'handles first page by excluding prev link' do
            base_url = request.base_url

            expect(jsonapi_pagination(Product.page(1))).to eql(
              self: base_url,
              first: "#{base_url}?page=1&page_size=5",
              next: "#{base_url}?page=2&page_size=5",
              last: "#{base_url}?page=2&page_size=5"
            )
          end

          it 'handles last page by excluding next link' do
            base_url = request.base_url

            expect(jsonapi_pagination(Product.page(2))).to eql(
              self: base_url,
              first: "#{base_url}?page=1&page_size=5",
              prev: "#{base_url}?page=1&page_size=5",
              last: "#{base_url}?page=2&page_size=5"
            )
          end
        end
      end
    end
  end
end
