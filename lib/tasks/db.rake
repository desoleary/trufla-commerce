# frozen_string_literal: true

namespace :db do
  desc 'Refreshes the database'
  task :refresh do
    abort('The Rails environment is running in production mode!') if Rails.env.production?

    # Clean and prep
    puts "Preparing Trufla #{Rails.env} database"

    puts 'Dropping schema.'
    Rake::Task['db:drop'].invoke

    puts 'Creating schema.'
    Rake::Task['db:create'].invoke

    puts 'Migrating.'
    Rake::Task['db:migrate'].invoke

    puts 'Dumping schema.'
    Rake::Task['db:schema:dump'].invoke

    puts 'Seeding database'
    Rake::Task['db:seed'].invoke

    puts "Trufla #{Rails.env} database has successfully been refreshed!"
  end
end
