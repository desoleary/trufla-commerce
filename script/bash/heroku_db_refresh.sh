#!/usr/bin/env bash

echo "Preparing Trufla database"

heroku pg:reset --confirm powerful-plateau-56437

heroku rake db:migrate

heroku rake db:seed

echo "Trufla database has successfully been refreshed"