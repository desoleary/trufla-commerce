# frozen_string_literal: true

# use awesome print for all objects in pry
begin
  require 'awesome_print'
  Pry.config.print = proc { |output, value| output.puts "=> #{ap value}" }
rescue StandardError
  puts '=> Unable to load awesome_print'
end

Pry.config.input = STDIN
Pry.config.output = STDOUT
