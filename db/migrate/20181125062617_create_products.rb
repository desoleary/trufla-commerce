# frozen_string_literal: true

class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name, index: :unique, limit: 255
      t.monetize :price, currency: { present: false }

      t.belongs_to :department, index: true, foreign_key: { on_update: :restrict, on_delete: :nullify }, null: false

      t.timestamps null: false
    end
  end
end
