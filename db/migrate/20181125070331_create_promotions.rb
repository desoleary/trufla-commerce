# frozen_string_literal: true

class CreatePromotions < ActiveRecord::Migration[5.2]
  def change
    create_table :promotions do |t|
      t.string :code, index: :unique, null: false, limit: 50
      t.integer :discount_percentage, default: 0, null: false
      t.datetime :starts_at
      t.datetime :expires_at

      t.timestamps
    end
  end
end
