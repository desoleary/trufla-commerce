# frozen_string_literal: true

class CreateDepartments < ActiveRecord::Migration[5.2]
  def change
    create_table :departments do |t|
      t.string :name, index: :unique, limit: 255

      t.timestamps
    end
  end
end
