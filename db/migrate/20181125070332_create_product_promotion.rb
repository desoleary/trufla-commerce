# frozen_string_literal: true

class CreateProductPromotion < ActiveRecord::Migration[5.2]
  def change
    create_table :product_promotion do |t|
      t.belongs_to :product, index: true, foreign_key: { on_update: :restrict, on_delete: :cascade }, null: false
      t.belongs_to :promotion, index: true, foreign_key: { on_update: :restrict, on_delete: :cascade }, null: false

      t.timestamps
    end

    add_index :product_promotion, %i[product_id promotion_id], unique: true
  end
end
