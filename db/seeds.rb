# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

department1 = FactoryBot.create(:department)
department2 = FactoryBot.create(:department)
department3 = FactoryBot.create(:department)
department4 = FactoryBot.create(:department)

promotion1 = FactoryBot.create(:promotion)
promotion2 = FactoryBot.create(:promotion)
promotion3 = FactoryBot.create(:promotion)
promotion4 = FactoryBot.create(:promotion)

FactoryBot.create(:product, department: department3)
FactoryBot.create_list(:product, 2, department: department1, promotion: promotion1)
FactoryBot.create_list(:product, 3, department: department2, promotion: promotion2)
FactoryBot.create(:product, department: department3, promotion: promotion3)
FactoryBot.create_list(:product, 4, department: department4, promotion: promotion4)
